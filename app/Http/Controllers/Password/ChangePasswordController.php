<?php

namespace App\Http\Controllers\Password;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function index()
    {
    	return view('layouts.change_password');
    }

    public function store()
    {
    	if (auth()->check()) {
	    	$this->validate(request(), [
	    		'old_password' => 'required|min:6',
	    		'password' => 'required|min:6|different:old_password|confirmed',
	    	]);

	    	$old_password = request('old_password');
	    	$password = bcrypt(request('password'));

	    	if (! Hash::check($old_password, auth()->user()->password)) {
	    		return back()->with('old_password_error', 'Old password is wrong');
	    	}

			auth()->user()->update(['password' => $password]);
			return back()->with('success', 'Password change success');
    	}

    	auth()->logout();
    	return redirect('/');
    }
}
