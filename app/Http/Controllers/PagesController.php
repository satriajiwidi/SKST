<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function root()
    {
		if (! auth()->check()) {
	    	return view('welcome');
		}

		return redirect('/home');
    }

    public function registerRestriction()
    {
    	return redirect('/');
    }

    public function logout()
    {
    	if (auth()->check()) {
    		auth()->logout();
    	}

    	return redirect('/');
    }
}
