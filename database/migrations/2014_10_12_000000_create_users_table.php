<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique()->nullable();
            $table->string('password');
            $table->boolean('is_admin')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });

        // Populating table.
        DB::table('users')->insert([
            [
                'name' => 'ini adalah admin',
                'username' => 'admin',
                'email' => 'admin@email.com',
                'password' => bcrypt('katakunci'),
                'is_admin' => true
            ],
            [
                'name' => 'ini adalah dosen',
                'username' => 'dosen',
                'email' => 'dosen@email.com',
                'password' => bcrypt('katakunci'),
                'is_admin' => false
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
