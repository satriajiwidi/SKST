<div class="panel panel-default">
    <div class="panel-heading">
        @yield('panel-heading')
    </div>
    <div class="panel-body">
        @yield('panel-body')
        Account:
        <br>
        # <a class="btn btn-link" href="{{ url('/password/change') }}">Change password</a>
        <br>
        # <a class="btn btn-link" href="{{ url('/logout') }}">Logout</a>
    </div>
</div>