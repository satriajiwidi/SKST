<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PagesController@root');
Route::get('/register', 'PagesController@registerRestriction');
Route::get('/logout', 'PagesController@logout');

Route::get('/home', 'HomeController@index');
Route::get('/password/change', 'Password\ChangePasswordController@index');
Route::post('/password/change', 'Password\ChangePasswordController@store');
